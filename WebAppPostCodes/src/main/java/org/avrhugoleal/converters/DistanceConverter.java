package org.avrhugoleal.converters;

import org.springframework.stereotype.Component;

import java.text.DecimalFormat;

@Component
public class DistanceConverter {

    private final double heathrowLatitude = 51.4700223;
    private final double heathrowLongitude = -0.4542955;
    private final double earthRadius = 6378.137;

    public String getDistance (double latitude, double longitude){

        double dLat = rad(latitude - heathrowLatitude);
        double dLong = rad(longitude - heathrowLongitude);

        double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(heathrowLatitude)) * Math.cos(rad(latitude)) * Math.sin(dLong/2) * Math.sin(dLong/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dKm = earthRadius * c;
        double dMiles = dKm * 0.621371192;

        DecimalFormat km = new DecimalFormat("#.0");
        DecimalFormat miles = new DecimalFormat("#.00");

        String distance = km.format(dKm) + " km / " + miles.format(dMiles) + " miles.";

        return distance;
    }

    private double rad (double dif){
        return dif * Math.PI/180;
    }

}
