package org.avrhugoleal.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.avrhugoleal.models.Address;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressDTO {

    private int status;
    private Address result;



    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Address getResult() {
        return result;
    }

    public void setResult(Address result) {
        this.result = result;
    }


}
