package org.avrhugoleal.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.avrhugoleal.converters.DistanceConverter;
import org.avrhugoleal.dtos.AddressDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/")
public class AddressController {

    private final String url = "https://api.postcodes.io/postcodes/";
    private final String sessionKey = "lastSearch";
    private DistanceConverter distanceConverter;

    @Autowired
    public void setDistanceConverter(DistanceConverter distanceConverter) {
        this.distanceConverter = distanceConverter;
    }

    @RequestMapping(method = RequestMethod.GET, path = {"/", ""})
    public String init(Model model, HttpServletRequest httpServletRequest) {

        List<AddressDTO> addressDTOList = getDtoList(httpServletRequest, null);
        httpServletRequest.getSession().setAttribute(sessionKey, addressDTOList);

        model.addAttribute("addressDTO", addressDTOList);
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/find")
    public String getAddress(Model model, @RequestParam("postcode") String pc, HttpServletRequest httpServletRequest) throws Exception {

        if (pc.isEmpty()) {
            return "error";
        }

        AddressDTO addressDTO = getAddressDto(pc);
        if (addressDTO == null) {
            return "error";
        }

        convertDistance(addressDTO);

        List<AddressDTO> addressDTOList = getDtoList(httpServletRequest, addressDTO);

        httpServletRequest.getSession().setAttribute(sessionKey, addressDTOList);

        model.addAttribute("addressDTO", addressDTOList);

        return "index";
    }

    private AddressDTO getAddressDto(String pc) {

        ObjectMapper mapper = new ObjectMapper();

        AddressDTO addressDTO;
        try {
            addressDTO = mapper.readValue(new URL(url + pc), AddressDTO.class);
        } catch (IOException e) {
            return null;
        }

        return addressDTO;
    }

    private void convertDistance(AddressDTO addressDTO) {
        String distance = distanceConverter.getDistance(addressDTO.getResult().getLatitude(), addressDTO.getResult().getLongitude());
        addressDTO.getResult().setDistance(distance);
    }

    private List<AddressDTO> getDtoList(HttpServletRequest httpServletRequest, AddressDTO addressDTO) {

        HttpSession session = httpServletRequest.getSession();

        List<AddressDTO> addressDTOList;
        addressDTOList = (List<AddressDTO>) session.getAttribute(sessionKey);
        if (addressDTOList == null) {
            addressDTOList = new LinkedList<>();
        }

        if (addressDTOList.size() > 2) {
            addressDTOList.remove(2);
        }

        List<AddressDTO> addressDTOListOrdered = new LinkedList<>();

        if (addressDTO != null) {
            addressDTOListOrdered.add(addressDTO);
        }

        addressDTOListOrdered.addAll(addressDTOList);

        return addressDTOListOrdered;
    }

}
